# Libfabric

## Building instructions
This repository contains the Libfabric builds used by FELIX.
Builds are obtained as follows:
1. for Centos7 use lxplus7, for AlmaLinux9 use a FELIX testbed host.\
 The reason is that `libefa.so.1`, shipped with `libibverbs`, is removed during the installation of MLNX_OFED >= 5.8, therefore `libefa.so.1` is present on lxplus but not on FELIX PCs. 
1. download the libfabric .tar.bz from https://github.com/ofiwg/libfabric/releases
1. set up a FELIX release so to load the gcc compiler
1. untar and execute 

>>>
./configure --prefix=/afs/cern.ch/user/d/duns/FELIX/felix-distribution/external/libfabric/1.13.1/x86_64-centos7-gcc8-opt --enable-psm=no --enable-psm2=no --enable-psm3=no --enable-usnic=no --enable-gni=no --enable-udp=no --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no --enable-mrail=yes --enable-rstream=no --enable-verbs=yes --enable-sockets=yes --enable-tcp=no

make

make install
>>>


The prefix option in the configure step corresponds to the installation path.\
All other flags enable and disable providers. The relevant flags for FELIX are the last three corresponding to the providers listed below.

| Provider | FELIX default | description |
| ---      | :---:     | ---      |
| verbs   | yes   | ibverbs provider for RDMA   |
| sockets | yes   | RDMA emulated over TCP/IP. Fallback solution for verbs.   |
| tcp     | no    | TCP/IP provider. Under testing as of Libfabric 1.18.1    |

## Enable debug information
Debug logging is controlled at run time by the environment variables `FI_LOG_LEVEL` and `FI_LOG_PROV`. 
However `FI_LOG_LEVEL=Debug` messages are acessible only if the build is run with the `--enable-debug` option.

## Test a new build
1. to make the FELIX software use the new version edit the [cmake_tdaq/bin/setup.sh](https://gitlab.cern.ch/atlas-tdaq-felix/cmake_tdaq/-/blob/master/bin/setup.sh).
2. recompile `felix-distribution` from scratch

## To update git
1. add the new build to this repo
2. commit the change in [cmake_tdaq/bin/setup.sh](https://gitlab.cern.ch/atlas-tdaq-felix/cmake_tdaq/-/blob/master/bin/setup.sh) if you want the new version to be the default.



# Notes of previous builds

#### Libfabric 1.4.1

use ibverbs 1.1.8 https://www.openfabrics.org/downloads/verbs/

    ./configure --prefix /afs/cern.ch/user/d/duns/FELIX/master/software/external/libfabric/1.4.1/x86_64-slc6-gcc49-opt
    make
    make install

use librdmacm 1.1.0 https://www.openfabrics.org/downloads/rdmacm/

    CFLAGS=-I/afs/cern.ch/user/d/duns/FELIX/master/software/external/libfabric/1.4.1/x86_64-slc6-gcc49-opt/include 
    LDFLAGS=-L/afs/cern.ch/user/d/duns/FELIX/master/software/external/libfabric/1.4.1/x86_64-slc6-gcc49-opt/lib ./configure --prefix /afs/cern.ch/user/d/duns/FELIX/master/software/external/libfabric/1.4.1/x86_64-slc6-gcc49-opt/
    make
    make install
 
use libfabric 1.4.1 https://ofiwg.github.io/libfabric/

    ./configure --prefix /afs/cern.ch/user/d/duns/FELIX/master/software/external/libfabric/1.4.1/x86_64-slc6-gcc49-opt/ --enable-verbs=/afs/cern.ch/user/d/duns/FELIX/master/software/external/libfabric/1.4.1/x86_64-slc6-gcc49-opt/ --enable-sockets=/afs/cern.ch/user/d/duns/FELIX/master/software/external/libfabric/1.4.1/x86_64-slc6-gcc49-opt/
    make 
    make install


#### Libfabric 1.6.1.2

Configured with:

    ./configure --prefix=/afs/cern.ch/work/j/joschuma/git/felix-rm4/external/libfabric/1.6.1/x86_64-slc6-gcc62-opt --enable-psm=no --enable-psm2=no --enable-sockets=no --enable-verbs=yes --enable-usnic=no --enable-mlx=no --enable-gni=no --enable-udp=no --enable-tcp=no --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no


#### Libfabric 1.7.1.3

Configured with:

    ./configure --prefix=/afs/cern.ch/work/j/joschuma/git/felix/external/libfabric/1.7.1.1/x86_64-centos7-gcc8-opt --enable-psm=no --enable-psm2=no --enable-sockets=yes --enable-verbs=yes --enable-usnic=no --enable-mlx=no --enable-gni=no --enable-udp=no --enable-tcp=no --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no --enable-mrail=yes

#### Libfabric 1.10.0

    ./configure --prefix=/afs/cern.ch/user/d/duns/tdaq/felix/felix-master/external/libfabric/1.10.0/x86_64-centos7-gcc8-opt  --enable-psm=no --enable-psm2=no --enable-sockets=yes --enable-verbs=yes --enable-usnic=no --enable-gni=no --enable-udp=no --enable-tcp=yes  --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no --enable-mrail=yes --enable-rstream=no

#### Libfabric 1.10.0.1, no TCP, compiled on pc-tbed-felix-04, against librdmacm 1.0.0

    ./configure --prefix=/afs/cern.ch/user/d/duns/tdaq/felix/felix-master/external/libfabric/1.10.0.1/x86_64-centos7-gcc8-opt  --enable-psm=no --enable-psm2=no --enable-sockets=yes --enable-verbs=yes --enable-usnic=no --enable-gni=no --enable-udp=no --enable-tcp=no  --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no --enable-mrail=yes --enable-rstream=no

#### Libfabric 1.10.0.2 no TCP, compiled on lxplus

    ./configure --prefix=/afs/cern.ch/user/d/duns/FELIX/master/software/external/libfabric/1.10.0.2/x86_64-centos7-gcc8-opt   --enable-psm=no --enable-psm2=no --enable-sockets=yes --enable-verbs=yes --enable-usnic=no --enable-gni=no --enable-udp=no --enable-tcp=no  --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no --enable-mrail=yes --enable-rstream=no

#### Libfabric 1.11.2, no TCP, compiled on lxplus, cmd re-taken from config.log, seems same as 1.10.0.2

    ./configure --prefix=/afs/cern.ch/user/d/duns/FELIX/master/software/external/libfabric/1.11.2/x86_64-centos7-gcc8-opt --enable-psm=no --enable-psm2=no --enable-sockets=yes --enable-verbs=yes --enable-usnic=no --enable-gni=no --enable-udp=no --enable-tcp=no  --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no --enable-mrail=yes --enable-rstream=no

#### Libfabric 1.12.0, no TCP, compiled on lxplus, cmd re-taken from config.log, seems same as 1.11.2

    ./configure --prefix=/afs/cern.ch/user/d/duns/FELIX/master/software/external/libfabric/1.12.0/x86_64-centos7-gcc8-opt --enable-psm=no --enable-psm2=no --enable-sockets=yes --enable-verbs=yes --enable-usnic=no --enable-gni=no --enable-udp=no --enable-tcp=no  --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no --enable-mrail=yes --enable-rstream=no

#### Libfabric 1.12.1, compiled on lxplus

    ./configure --prefix=/afs/cern.ch/work/c/cgottard/FELIX/software-dev/external/libfabric/1.12.1/x86_64-centos7-gcc8-opt --enable-psm=no --enable-psm2=no --enable-psm3=no --enable-sockets=yes --enable-verbs=yes --enable-usnic=no --enable-gni=no --enable-udp=no --enable-tcp=no --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no --enable-mrail=yes --enable-rstream=no

#### Libfabric 1.13.1, compiled on lxplus

    ./configure --prefix=/afs/cern.ch/user/d/duns/FELIX/felix-distribution-42/external/libfabric/1.13.1/x86_64-centos7-gcc8-opt --enable-psm=no --enable-psm2=no --enable-psm3=no --enable-sockets=yes --enable-verbs=yes --enable-usnic=no --enable-gni=no --enable-udp=no --enable-tcp=no --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no --enable-mrail=yes --enable-rstream=no

